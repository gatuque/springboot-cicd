#define a base docker file image
FROM openjdk:17-alpine
LABEL maintainer="g-coders.com"

ADD  target/employee-REST-API-0.0.1-SNAPSHOT.jar my-app.jar
ENTRYPOINT ["java", "-jar", "my-app.jar"]