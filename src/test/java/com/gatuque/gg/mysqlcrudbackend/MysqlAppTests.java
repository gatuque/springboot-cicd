package com.gatuque.gg.mysqlcrudbackend;

import com.gatuque.gg.mysqlcrudbackend.model.Employee;
import com.gatuque.gg.mysqlcrudbackend.repository.EmployeeRepository;
import com.gatuque.gg.mysqlcrudbackend.service.EmployeeService;
import com.gatuque.gg.mysqlcrudbackend.service.EmployeeServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
public class MysqlAppTests {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeServiceImpl employeeService;


    @Test
    public void addEmployeeTest(){
        when(employeeRepository.findAll()).thenReturn(
                Stream.of(new Employee(1L, "John", "Doe", "johnDoe@gmail.com"),
                         new Employee(2L,"Jane","Doe","janeDoe@gmail.com")).collect(Collectors.toList()));

        assertEquals(2, employeeService.getEmployees().size());
    }
}
