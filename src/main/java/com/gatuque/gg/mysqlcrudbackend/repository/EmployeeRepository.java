package com.gatuque.gg.mysqlcrudbackend.repository;

import com.gatuque.gg.mysqlcrudbackend.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

//This annotation creates a spring Data REST api
//@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
