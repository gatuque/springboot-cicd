package com.gatuque.gg.mysqlcrudbackend.controller;

import com.gatuque.gg.mysqlcrudbackend.model.Employee;
import com.gatuque.gg.mysqlcrudbackend.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(@Qualifier("getEmployees2") EmployeeService employeeService) {
        super();
        this.employeeService = employeeService;
    }

    // build create employee REST API
    @PostMapping()
    public ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee){
        //return new ResponseEntity<>(employeeService.saveEmployee(employee), HttpStatus.CREATED);
        return ResponseEntity.ok(employeeService.saveEmployee(employee));
    }

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Employee> getEmployees(){
        return employeeService.getEmployees();
    }

    //Get employee by ID
    @GetMapping(path = "{employeeID}")
    public ResponseEntity<Employee> getEmployee(@PathVariable long employeeID){
        return new ResponseEntity<>(employeeService.getEmployee(employeeID), HttpStatus.OK);
    }

    //http://localhost:8080/api/employees?id=1&lastname=kinuthia
    @PutMapping
    public Employee updateEmployeeLastName(@RequestParam long id, @RequestParam String lastname){
        return employeeService.updateEmployeeLastName(id, lastname);
    }

    @DeleteMapping(path = "{employeeId}")
    ResponseEntity<String> deleteEmployee(@PathVariable long employeeId){
        return new ResponseEntity<>(employeeService.deleteEmployee(employeeId), HttpStatus.OK);
    }

    //INTRODUCING VIEW
//    @GetMapping()
//    public String listEmployees(Model model){
//        model.addAttribute("employees", employeeService.getEmployees());
//        return "employees";
//
//    }
}

