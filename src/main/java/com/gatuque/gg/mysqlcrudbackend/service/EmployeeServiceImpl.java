package com.gatuque.gg.mysqlcrudbackend.service;

import com.gatuque.gg.mysqlcrudbackend.exception.ResourceNotFoundException;
import com.gatuque.gg.mysqlcrudbackend.model.Employee;
import com.gatuque.gg.mysqlcrudbackend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("getEmployees1")
public class EmployeeServiceImpl implements EmployeeService{

    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getEmployees() {
        System.out.println("Retrieving Employees from EmployeeServiceImpl");
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployee(long employeeID) {
//        Optional<Employee> employee = employeeRepository.findById(employeeID);
//        if (employee.isPresent()){
//            return employee.get();
//        }else {
//            throw new ResourceNotFoundException("Employee","ID",employeeID);
//        }

        // using a lambda
        return employeeRepository.findById(employeeID).
                orElseThrow(() -> new ResourceNotFoundException("Employee","ID",employeeID));
    }

    @Override
    public Employee updateEmployeeLastName(long id, String lastname) {
        Employee employee = employeeRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Employee" ,"id",id));

        employee.setLastname(lastname);
        employeeRepository.save(employee);
        return employee;
    }

    @Override
    public String deleteEmployee(long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("deleteEmployee", "ID", id));
        employeeRepository.deleteById(id);

        return employee.getFirstname() + " has been deleted ok";
    }


}
