package com.gatuque.gg.mysqlcrudbackend.service;

import com.gatuque.gg.mysqlcrudbackend.model.Employee;

import java.util.List;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);

    List<Employee> getEmployees();

    Employee getEmployee(long employeeID);

    Employee updateEmployeeLastName(long id,String lastname);

    String deleteEmployee(long id);
}
