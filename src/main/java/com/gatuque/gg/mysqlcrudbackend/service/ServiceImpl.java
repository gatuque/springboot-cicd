package com.gatuque.gg.mysqlcrudbackend.service;

import com.gatuque.gg.mysqlcrudbackend.model.Employee;
import com.gatuque.gg.mysqlcrudbackend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Qualifier("getEmployees2")
public class ServiceImpl implements EmployeeService{
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return null;
    }

    @Override
    public List<Employee> getEmployees() {
        System.out.println("Retrieving Employees from ServiceImpl");
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployee(long employeeID) {
        return null;
    }

    @Override
    public Employee updateEmployeeLastName(long id, String lastname) {
        return null;
    }

    @Override
    public String deleteEmployee(long id) {
        return null;
    }
}
